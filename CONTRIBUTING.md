# Contributing

We welcome community contributions for bug-fixes, improvements, and suggestions. 

## Our priorities

1. Reliablity - GridTracker and it's features as implemented should run reliablly on all supported platforms. Changes should be tested on all platforms and on as many user configurations as possible before being released to the general public.
2. No Regressions - GridTracker v1 has had issues in the past with regressions. Fixing or adding something breaks somthing else. Fixing the stuff we broke breaks the thing we originally fixed or breaks what we added. Our goal is to be regression free in v2.
3. Iterative Development - GridTracker changes should be iterative with constant testing from the shakedown crew and feedback from the general userbase on the impementation of features in the development cycle.
4. Progressive - GridTracker should be leading in how amateurs interface with the plethra of hamshack software packages. We should work with all the other software developers to integrate with them, and enhance the modern hamshack and enhance our partner's software to their users. 
5. Do Best What We Do Best - GridTracker is not in itself a logging program because there are already so many in use that we integrate with. Likewise we don't fork WSJT-X or FLdigi or other digital radio modem packages for the same reason. GridTracker should excel at being a visual aid to operating and the unifying glue to connect everything together. 

## Issues

Please submit bug reports, feature requests and suggestions to the [issue tracker](https://gitlab.com/gridtracker.org/project-squirrel/-/issues) for this project. We ask that you fill out the issue request using the default form
```md
# Summary
(Summarize the bug encountered concisely)

# Version
(which version of code is running)

# Steps to reproduce
(How one can reproduce the issue - this is very important)

# What is the current bug behavior?
(What actually happens)

# What is the expected correct behavior?
(What you should see instead)

# Relevant logs and/or screenshots
(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

# Possible fixes
(If you can, link to the line of code that might be responsible for the problem)

# This is labled as (remove all but the applicable)
/label ~bug ~new-feature ~suggstion ~discussion
```
And importantly to delete non-applicable labels. 

## Code Style

GridTracker for readability by all our developers and to make reviewing code and keeping an accurate history of changes uses a specific and enforced code style. 

Code in GridTracker v2 is linted against [JavaScript Standard Style](https://standardjs.com/). 