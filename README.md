# Project Squirrel

## What is this?

Project Squirrel is the next gen version of GridTracker. This is where we're migrating stuff into Electron so we can stay up with the rapidly evolving demands of the platforms that GridTracker is released to and gives us the possiblity to include more platforms going forward.

## Why is it called Project Squirrel

Because this has been a long discussed idea that has been tossed about since before GridTracker was released open source. But because of just trying to keep up with fixes and hotly requested features in GridTracker v1 and keeping life/work/hobby balance we have kicked this down the road until now. We say this is the squirrel we've been trying to chase but keep getting our attention diverted. 

But why now? Well we've hit a point where maintaining v1 is starting to look very difficult because of all the stuff that's not included or barely works in NWJS. There's a lot of stuff we can do more efficiently in electron and why it's taking increasingly longer to roll out updates when we're fighting our own ecosystem to make things work. 

## Contributing

Please see the [CONTRIBUTING.md](https://gitlab.com/gridtracker.org/project-squirrel/-/blob/main/CONTRIBUTING.md) file for details on how you the community can contirbute to GridTracker v2