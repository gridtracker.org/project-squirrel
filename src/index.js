const { app, BrowserWindow } = require('electron')

const pjson = require('package.json')
const gtVerStr = pjson.version
const gtBeta = pjson.betaVersion

console.log(gtVerStr + ' ' + gtBeta)

app.on('ready', () => {
  const window = new BrowserWindow({ width: 800, height: 600 })
  window.setMenuBarVisibility(true)
  window.loadURL('https://google.com')
})
