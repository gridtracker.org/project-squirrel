function loadMaidenHeadData () {
  // load mh-root-prefixed.json into memory
  if (fs.existsSync('assets/data/mh-root-prefixed.json')) {
    let fileBuf = fs.readFileSync('assets/data/mh-root-prefixed.json', 'UTF-8')
    gWorldGeoData = JSON.parse(fileBuf)

    for (const key in gWorldGeoData) {
      gWorldGeoData[key].geo = 'deleted'
      gWorldGeoData[key].worked_bands = {}
      gWorldGeoData[key].confirmed_bands = {}
      gWorldGeoData[key].worked_modes = {}
      gWorldGeoData[key].confirmed_modes = {}
      gDxccToAltName[gWorldGeoData[key].dxcc] = gWorldGeoData[key].name
      gDxccToADIFName[gWorldGeoData[key].dxcc] = gWorldGeoData[key].aname
      gDxccToGeoData[gWorldGeoData[key].dxcc] = key

      for (let x = 0; x < gWorldGeoData[key].prefix.length; x++) {
        if (gWorldGeoData[key].prefix[x].charAt(0) === '=') {
          gDirectCallToDXCC[gWorldGeoData[key].prefix[x].substr(1)] =
            gWorldGeoData[key].dxcc
        } else {
          gPrefixToMap[gWorldGeoData[key].prefix[x]] = key
        }
      }
      delete gWorldGeoData[key].prefix
      for (let x = 0; x < gWorldGeoData[key].mh.length; x++) {
        if (!(gWorldGeoData[key].mh[x] in gGridToDXCC)) {
          gGridToDXCC[gWorldGeoData[key].mh[x]] = Array()
        }
        gGridToDXCC[gWorldGeoData[key].mh[x]].push(gWorldGeoData[key].dxcc)
      }

      if (gWorldGeoData[key].dxcc !== 291 && gWorldGeoData[key].dxcc !== 110 && gWorldGeoData[key].dxcc !== 6) {
        delete gWorldGeoData[key].mh
      }
    }

    // load dxcc.json into memory
    let files = fs.readFileSync('assets/data/dxcc.json')
    const dxccGeo = JSON.parse(files)

    for (const key in dxccGeo.features) {
      const dxcc = dxccGeo.features[key].properties.dxcc_entity_code
      gWorldGeoData[gDxccToGeoData[dxcc]].geo = dxccGeo.features[key]
    }

    // load counties.json into memory
    files = fs.readFileSync('assets/data/counties.json')
    let countyData = JSON.parse(files)

    for (const id in countyData) {
      if (!(countyData[toString(id)].properties.st in gStateToCounty)) {
        gStateToCounty[countyData[toString(id)].properties.st] = Array()
      }
      gStateToCounty[countyData[toString(id)].properties.st].push(id)

      const cnty =
        countyData[toString(id)].properties.st +
        ',' +
        countyData[toString(id)].properties.n.toUpperCase().replaceAll(' ', '')
      if (!(cnty in gCntyToCounty)) {
        gCntyToCounty[toString(cnty)] = countyData[toString(id)].properties.n.toProperCase()
      }
      gCountyData[toString(cnty)] = {}
      gCountyData[toString(cnty)].geo = countyData[toString(id)]
      gCountyData[toString(cnty)].worked = false
      gCountyData[toString(cnty)].confirmed = false
      gCountyData[toString(cnty)].worked_bands = {}
      gCountyData[toString(cnty)].confirmed_bands = {}
      gCountyData[toString(cnty)].worked_modes = {}
      gCountyData[toString(cnty)].confirmed_modes = {}

      for (const x in countyData[toString(id)].properties.z) {
        const zips = String(countyData[toString(id)].properties.z[x])
        if (!(zips in gZipToCounty)) {
          gZipToCounty[toInt(zips)] = Array()
        }
        gZipToCounty[toInt(zips)].push(cnts)
      }
    }
    files = null
    countyData = null

    // load shapes.json into memory
    gShapeData = JSON.parse(fs.readFileSync('assets/data/shapes.json'))
    for (const key in gShapeData) {
      if (gShapeData[key].properties.alias === key) {
        gShapeData[key].properties.alias = null
      } else if (
        gShapeData[key].properties.alias &&
        gShapeData[key].properties.alias.length > 2 &&
        (gShapeData[key].properties.alias.indexOf('US') === 0 ||
          gShapeData[key].properties.alias.indexOf('CA') === 0)
      ) {
        gShapeData[key].properties.alias = null
      }
      if (
        gShapeData[key].properties.alias &&
        gShapeData[key].properties.alias.length < 2
      ) {
        gShapeData[key].properties.alias = null
      }
      if (gShapeData[key].properties.alias !== null) {
        if (key.indexOf('CN-') === 0) {
          if (gShapeData[key].properties.alias == key.replace('CN-', '')) {
            gShapeData[key].properties.alias = null
          }
        }
      }
      if (
        gShapeData[key].properties.alias !== null &&
        gShapeData[key].properties.alias.length !== 2
      ) {
        gShapeData[key].properties.alias = null
      }
    }

    // finalDxcc == 291 || finalDxcc == 110 || finalDxcc == 6
    // Create "US" shape from US Dxcc geos
    const x = gWorldGeoData[gDxccToGeoData[291]].geo.geometry
    let y = gShapeData.AK.geometry
    let z = gShapeData.HI.geometry

    let feature = {
      type: 'Feature',
      geometry: {
        type: 'GeometryCollection',
        geometries: [x, y, z]
      },
      properties: {
        name: 'United States',
        center: gWorldGeoData[gDxccToGeoData[291]].geo.properties.center,
        postal: 'US',
        type: 'Country'
      }
    }
    gShapeData.US = feature

    y = gShapeData.OC.geometry
    z = gShapeData.AU.geometry
    q = gShapeData.AN.geometry

    feature = {
      type: 'Feature',
      geometry: {
        type: 'GeometryCollection',
        geometries: [y, z, q]
      },
      properties: {
        name: 'Oceania',
        center: [167.97602, -29.037824],
        postal: 'OC',
        type: 'Continent'
      }
    }
    gShapeData.OC = feature
    gShapeData.AU.properties.type = 'Country'

    // load state.json into memory
    gStateData = JSON.parse(fs.readFileSync('assets/data/state.json'))

    gStateData['US-AK'] = {}
    gStateData['US-AK'].postal = 'US-AK'
    gStateData['US-AK'].name = 'Alaska'
    gStateData['US-AK'].mh = gWorldGeoData[5].mh
    gStateData['US-AK'].dxcc = 6

    gStateData['US-HI'] = {}
    gStateData['US-HI'].postal = 'US-HI'
    gStateData['US-HI'].name = 'Hawaii'
    gStateData['US-HI'].mh = gWorldGeoData[100].mh
    gStateData['US-HI'].dxcc = 110

    for (const key in gStateData) {
      for (let x = 0; x < gStateData[key].mh.length; x++) {
        if (!(gStateData[key].mh[x] in gGridToState)) {
          gGridToState[gStateData[key].mh[x]] = Array()
        }
        gGridToState[gStateData[key].mh[x]].push(gStateData[key].postal)
      }
      gStateData[key].worked_bands = {}
      gStateData[key].confirmed_bands = {}
      gStateData[key].worked_modes = {}
      gStateData[key].confirmed_modes = {}
    }
    // load phone.json into memmory
    fileBuf = fs.readFileSync('assets/data/phone.json', 'UTF-8')
    gPhonetics = JSON.parse(fileBuf)

    // load enums.json into memory
    fileBuf = fs.readFileSync('assets/data/enums.json', 'UTF-8')
    gEnums = JSON.parse(fileBuf)

    for (const key in gWorldGeoData) {
      if (
        gWorldGeoData[key].pp !== '' &&
        gWorldGeoData[key].geo !== 'deleted'
      ) {
        gEnums[gWorldGeoData[key].dxcc] = gWorldGeoData[key].name
      }
      if (key === 270) {
        // US Mainland
        for (const mh in gWorldGeoData[key].mh) {
          const sqr = gWorldGeoData[key].mh[mh]

          gUs48Data[textToGrid(sqr)] = {}
          gUs48Data[textToGrid(sqr)].name = sqr
          gUs48Data[textToGrid(sqr)].worked = false
          gUs48Data[textToGrid(sqr)].confirmed = false
          gUs48Data[textToGrid(sqr)].worked_bands = {}
          gUs48Data[textToGrid(sqr)].confirmed_bands = {}
          gUs48Data[textToGrid(sqr)].worked_modes = {}
          gUs48Data[textToGrid(sqr)].confirmed_modes = {}
        }
      }
    }

    // load cqzone.json into memory
    fileBuf = fs.readFileSync('assets/data/cqzone.json')
    gCqZones = JSON.parse(fileBuf)

    for (const key in gCqZones) {
      for (let x = 0; x < gCqZones[key].mh.length; x++) {
        if (!(gCqZones[key].mh[x] in gGridToCQZone)) {
          gGridToCQZone[gCqZones[key].mh[x]] = Array()
        }
        gGridToCQZone[gCqZones[key].mh[x]].push(String(key))
      }
      delete gCqZones[key].mh
    }

    // load ituzone.json into memory
    fileBuf = fs.readFileSync('assets/data/ituzone.json')
    gItuZones = JSON.parse(fileBuf)

    for (const key in gItuZones) {
      for (let x = 0; x < gItuZones[toString(key)].mh.length; x++) {
        if (!(gItuZones[toString(key)].mh[x] in gGridToITUZone)) {
          gGridToITUZone[gItuZones[toString(key)].mh[x]] = Array()
        }
        gGridToITUZone[gItuZones[toString(key)].mh[x]].push(String(key))
      }
      delete gItuZones[toString(key)].mh
    }

    for (const key in gStateData) {
      if (key.substr(0, 3) === 'US-') {
        const shapeKey = key.substr(3, 2)
        const name = gStateData[key].name

        if (shapeKey in gShapeData) {
          gWasZones[toString(name)] = {}
          gWasZones[toString(name)].geo = gShapeData[shapeKey]
          gWasZones[toString(name)].worked = false
          gWasZones[toString(name)].confirmed = false

          gWasZones[toString(name)].worked_bands = {}
          gWasZones[toString(name)].confirmed_bands = {}
          gWasZones[toString(name)].worked_modes = {}
          gWasZones[toString(name)].confirmed_modes = {}
        }
      }
    }
    let name = 'Alaska'
    let shapeKey = 'AK'
    gWasZones[toString(name)] = {}
    gWasZones[toString(name)].geo = gShapeData[shapeKey]

    gWasZones[toString(name)].worked = false
    gWasZones[toString(name)].confirmed = false

    gWasZones[toString(name)].worked_bands = {}
    gWasZones[toString(name)].confirmed_bands = {}
    gWasZones[toString(name)].worked_modes = {}
    gWasZones[toString(name)].confirmed_modes = {}

    name = 'Hawaii'
    shapeKey = 'HI'
    gWasZones[toString(name)] = {}
    gWasZones[toString(name)].geo = gShapeData[shapeKey]

    gWasZones[toString(name)].worked = false
    gWasZones[toString(name)].confirmed = false

    gWasZones[toString(name)].worked_bands = {}
    gWasZones[toString(name)].confirmed_bands = {}
    gWasZones[toString(name)].worked_modes = {}
    gWasZones[toString(name)].confirmed_modes = {}

    for (const key in gShapeData) {
      if (gShapeData[key].properties.type === 'Continent') {
        const name = gShapeData[key].properties.name
        gWacZones[toString(name)] = {}
        gWacZones[toString(name)].geo = gShapeData[key]

        gWacZones[toString(name)].worked = false
        gWacZones[toString(name)].confirmed = false

        gWacZones[toString(name)].worked_bands = {}
        gWacZones[toString(name)].confirmed_bands = {}
        gWacZones[toString(name)].worked_modes = {}
        gWacZones[toString(name)].confirmed_modes = {}
      }
    }
  }
}

function createZoneLayer () {
  const gTimezoneLayer = createGeoJsonLayer(
    'tz',
    'assets/data/combined-with-oceans.json',
    '#000088FF',
    0.5
  )
  gMap.addLayer(gTimezoneLayer)
  gTimezoneLayer.setVisible(false)
}
