// GridTracker Copyright © 2022 GridTracker.org
// All rights reserved.
// See LICENSE for more information.

let gPotaPlaces = null
let gPotaSpots = null

function ingestPotaPlaces (buffer) {
  try {
    gPotaPlaces = JSON.parse(buffer)
  } catch (e) {
    // can't write, somethings broke
  }
}

function getPotaPlaces () {
  if (gMapSettings.offlineMode === false) {
    getBuffer(
      'https://storage.googleapis.com/gt_app/pota.json',
      ingestPotaPlaces,
      null,
      'https',
      443
    )

    setTimeout(getPotaPlaces, 86400000)
  }
}

function ingestPotaSpots (buffer) {
  try {
    gPotaSpots = JSON.parse(buffer)
  } catch (e) {
    // can't write, somethings broke
  }
}

function getPotaSpots () {
  if (gMapSettings.offlineMode === false && gSpotsEnabled === 1) {
    getBuffer(
      'https://api.pota.app/spot/activator',
      ingestPotaSpots,
      null,
      'https',
      443
    )

    setTimeout(getPotaSpots, 300000)
  }
}

function gSendPotaSpot () {
  // if Pota spotting enabled, and we have enough info, send a spot to Pota
}
