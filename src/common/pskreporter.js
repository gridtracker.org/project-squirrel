function pskBandActivityCallback (buffer, flag) {
  const result = String(buffer)
  if (result.indexOf('frequency score') > -1) {
    // looks good so far
    gBandActivity.lines[myMode] = result.split('\n')
    gBandActivity.lastUpdate[myMode] = gTimeNow + 600
    localStorage.bandActivity = JSON.stringify(gBandActivity)
  }
  renderBandActivity()
}

function pskGetBandActivity () {
  if (gMapSettings.offlineMode === true) {
    return
  }
  if (typeof gBandActivity.lastUpdate[myMode] === 'undefined') {
    gBandActivity.lastUpdate[myMode] = 0
  }

  if (myMode.length > 0 && myDEGrid.length > 0 && gTimeNow > gBandActivity.lastUpdate[myMode]) {
    getBuffer(
      'https://pskreporter.info/cgi-bin/psk-freq.pl?mode=' +
      myMode +
      '&grid=' +
      myDEGrid.substr(0, 4),
      pskBandActivityCallback,
      null,
      'https',
      443
    )
  }
  renderBandActivity()
  if (gPskBandActivityTimerHandle != null) {
    clearInterval(gPskBandActivityTimerHandle)
  }

  gPskBandActivityTimerHandle = setInterval(function () {
    pskGetBandActivity()
  }, 601000) // every 20 minutes, 1 second
}
