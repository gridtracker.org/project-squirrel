function colorToHex (color) {
  if (color.substr(0, 1) === '#') {
    return color
  }
  const digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color)
  const red = parseInt(digits[2])
  const green = parseInt(digits[3])
  const blue = parseInt(digits[4])
  let rgb = ('00' + (+red).toString(16)).substr(-2)
  rgb += ('00' + (+green).toString(16)).substr(-2)
  rgb += ('00' + (+blue).toString(16)).substr(-2)
  return '#' + rgb
}

function setHueColor () {
  gMapHue = colorToHex(hueDiv.style.backgroundColor)
  if (gMapHue === '#000000') {
    gMapHue = 0
  }
}

function rgbToHex (R, G, B) {
  return toHex(R) + toHex(G) + toHex(B)
}

function toHex (n) {
  n = parseInt(n, 10)
  if (isNaN(n)) {
    return '00'
  }
  n = Math.max(0, Math.min(n, 255))
  return (
    '0123456789ABCDEF'.charAt((n - (n % 16)) / 16) +
    '0123456789ABCDEF'.charAt(n % 16)
  )
}

function hexToR (h) {
  return parseInt(cutHex(h).substring(0, 2), 16)
}

function hexToG (h) {
  return parseInt(cutHex(h).substring(2, 4), 16)
}

function hexToB (h) {
  return parseInt(cutHex(h).substring(4, 6), 16)
}

function hexToA (h) {
  return parseInt(cutHex(h).substring(6, 8), 16)
}

function cutHex (h) {
  return h.charAt(0) == '#' ? h.substring(1, 9) : h
}
