function timeoutSetUdpPort () {
  gAppSettings.wsjtUdpPort = udpPortInput.value
  lastMsgTimeDiv.innerHTML = 'Waiting for msg...'
  gSetNewUdpPortTimeoutHandle = null
}

function setUdpPort () {
  if (gSetNewUdpPortTimeoutHandle != null) {
    window.clearTimeout(gSetNewUdpPortTimeoutHandle)
  }
  lastMsgTimeDiv.innerHTML = '..setting..'
  gSetNewUdpPortTimeoutHandle = window.setTimeout(timeoutSetUdpPort, 1000)
}

function setMulticastIp () {
  gAppSettings.wsjtIP = multicastIpInput.value
}

function setMulticastEnable (checkbox) {
  if (checkbox.checked) {
    multicastTD.style.display = 'block'
    if (ValidateMulticast(multicastIpInput)) {
      gAppSettings.wsjtIP = multicastIpInput.value
    } else {
      gAppSettings.wsjtIP = ''
    }
  } else {
    multicastTD.style.display = 'none'
    gAppSettings.wsjtIP = ''
  }
  gAppSettings.multicast = checkbox.checked
}

function setUdpForwardEnable (checkbox) {
  if (checkbox.checked) {
    if (
      validatePort(
        udpForwardPortInput,
        null,
        CheckForwardPortIsNotReceivePort
      ) &&
      ValidateIPaddress(udpForwardIpInput, null)
    ) {
      gAppSettings.wsjtForwardUdpEnable = checkbox.checked
      return
    }
  }
  checkbox.checked = false
  gAppSettings.wsjtForwardUdpEnable = checkbox.checked
}

function CheckReceivePortIsNotForwardPort (value) {
  if (
    udpForwardIpInput.value === '127.0.0.1' &&
    udpForwardPortInput.value === value &&
    gAppSettings.wsjtIP === '' &&
    udpForwardEnable.checked
  ) {
    return false
  }
  return true
}

function CheckForwardPortIsNotReceivePort (value) {
  if (
    udpForwardIpInput.value === '127.0.0.1' &&
    udpPortInput.value === value &&
    gAppSettings.wsjtIP === ''
  ) {
    return false
  }
  return true
}

function setForwardIp () {
  gAppSettings.wsjtForwardUdpIp = udpForwardIpInput.value
  if (ValidatePort(udpPortInput, null, CheckReceivePortIsNotForwardPort)) {
    setUdpPort()
  }
  ValidatePort(udpForwardPortInput, null, CheckForwardPortIsNotReceivePort)
}

function setForwardPort () {
  gAppSettings.wsjtForwardUdpPort = udpForwardPortInput.value
  ValidateIPaddress(udpForwardIpInput, null)
  if (ValidatePort(udpPortInput, null, CheckReceivePortIsNotForwardPort)) {
    setUdpPort()
  }
}

function validIpKeys (value) {
  if (value === 46) {
    return true
  }
  return value >= 48 && value <= 57
}

function ipToInt (ip) {
  return ip
    .split('.')
    .map((octet, index, array) => {
      return parseInt(octet) * Math.pow(256, array.length - index - 1)
    })
    .reduce((prev, curr) => {
      return prev + curr
    })
}

function ValidateMulticast (inputText) {
  const ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
  if (inputText.value.match(ipformat)) {
    if (inputText.value !== '0.0.0.0' && inputText.value !== '255.255.255.255') {
      const ipInt = ipToInt(inputText.value)
      if (ipInt >= ipToInt('224.0.0.0') && ipInt < ipToInt('240.0.0.0')) {
        if (ipInt > ipToInt('224.0.0.255')) {
          inputText.style.color = 'black'
          inputText.style.backgroundColor = 'yellow'
        } else {
          inputText.style.color = '#FF0'
          inputText.style.backgroundColor = 'green'
        }
        return true
      } else {
        inputText.style.color = '#FFF'
        inputText.style.backgroundColor = 'red'
        return false
      }
    } else {
      inputText.style.color = '#FFF'
      inputText.style.backgroundColor = 'red'
      return false
    }
  } else {
    inputText.style.color = '#FFF'
    inputText.style.backgroundColor = 'red'
    return false
  }
}

function ValidateIPaddress (inputText, checkBox) {
  const ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
  if (inputText.value.match(ipformat)) {
    if (inputText.value !== '0.0.0.0' && inputText.value !== '255.255.255.255') {
      inputText.style.color = '#FF0'
      inputText.style.backgroundColor = 'green'
      return true
    } else {
      inputText.style.color = '#FFF'
      inputText.style.backgroundColor = 'red'
      if (checkBox) {
        checkBox.checked = false
      }
      return false
    }
  } else {
    inputText.style.color = '#FFF'
    inputText.style.backgroundColor = 'red'
    if (checkBox) {
      checkBox.checked = false
    }
    return false
  }
}

function ValidatePort (inputText, checkBox, callBackCheck) {
  const value = Number(inputText.value)
  if (value > 1023 && value < 65536) {
    if (callBackCheck && !callBackCheck(value)) {
      inputText.style.color = '#FFF'
      inputText.style.backgroundColor = 'red'
      if (checkBox) {
        checkBox.checked = false
      }
      return false
    } else {
      inputText.style.color = '#FF0'
      inputText.style.backgroundColor = 'green'
      return true
    }
  } else {
    inputText.style.color = '#FFF'
    inputText.style.backgroundColor = 'red'
    if (checkBox) {
      checkBox.checked = false
    }
    return false
  }
}

function loadPortSettings () {
  multicastEnable.checked = gAppSettings.multicast
  multicastIpInput.value = gAppSettings.wsjtIP
  setMulticastEnable(multicastEnable)
  udpPortInput.value = gAppSettings.wsjtUdpPort
  ValidatePort(udpPortInput, null, CheckReceivePortIsNotForwardPort)
  udpForwardPortInput.value = gAppSettings.wsjtForwardUdpPort
  ValidatePort(udpForwardPortInput, null, CheckForwardPortIsNotReceivePort)
  udpForwardIpInput.value = gAppSettings.wsjtForwardUdpIp
  ValidateIPaddress(udpForwardIpInput, null)
  udpForwardEnable.checked = gAppSettings.wsjtForwardUdpEnable
  setUdpForwardEnable(udpForwardEnable)
}

const gWsjtCurrentPort = -1
const gWsjtUdpServer = null
const gWsjtUdpSocketReady = false
const gWsjtUdpSocketError = false

let gForwardUdpServer = null

function updateForwardListener () {
  if (gForwardUdpServer !== null) {
    gForwardUdpServer.close()
  }
  if (gClosing === true) {
    return
  }

  const dgram = require('dgram')
  gForwardUdpServer = dgram.createSocket({
    type: 'udp4',
    reuseAddr: true
  })

  gForwardUdpServer.on('listening', function () { })
  gForwardUdpServer.on('error', function () {
    gForwardUdpServer.close()
    gForwardUdpServer = null
  })
  gForwardUdpServer.on('message', function (originalMessage, remote) {
    // Decode enough to get the rig-name, so we know who to send to
    let message = Object.assign({}, originalMessage)
    const newMessage = {}
    newMessage.magic_key = decodeQUINT32(message)
    message = message.slice(gQtToSplice)
    if (newMessage.magic_key === 0xadbccbda) {
      newMessage.schema_number = decodeQUINT32(message)
      message = message.slice(gQtToSplice)
      newMessage.type = decodeQUINT32(message)
      message = message.slice(gQtToSplice)
      newMessage.Id = decodeQUTF8(message)
      message = message.slice(gQtToSplice)

      const instanceId = newMessage.Id
      if (instanceId in gInstances) {
        wsjtUdpMessage(
          originalMessage,
          originalMessage.length,
          gInstances[toString(instanceId)].remote.port,
          gInstances[toString(instanceId)].remote.address
        )
      }
    }
  })
  gForwardUdpServer.bind(0)
}

function sendForwardUdpMessage (msg, length, port, address) {
  if (gForwardUdpServer) {
    gForwardUdpServer.send(msg, 0, length, port, address)
  }
}

function wsjtUdpMessage (msg, length, port, address) {
  if (gWsjtUdpServer) {
    gWsjtUdpServer.send(msg, 0, length, port, address)
  }
}

function checkWsjtxListener () {
  if (gWsjtUdpServer === null || (gWsjtUdpSocketReady === false && gWsjtUdpSocketError === true)) {
    gWsjtCurrentPort = -1
    gWsjtCurrentIP = 'none'
  }
  updateWsjtxListener(gAppSettings.wsjtUdpPort)
}

const gInstances = {}
const gInstancesIndex = Array()

const gActiveInstance = ''
const gActiveIndex = 0

const gCurrentID = null

function updateWsjtxListener (port) {
  if (port === gWsjtCurrentPort && gAppSettings.wsjtIP === gWsjtCurrentIP) {
    return
  }
  if (gWsjtUdpServer !== null) {
    if (multicastEnable.checked === true && gAppSettings.wsjtIP !== '') {
      try {
        gWsjtUdpServer.dropMembership(gAppSettings.wsjtIP)
      } catch (e) { }
    }
    gWsjtUdpServer.close()
    gWsjtUdpServer = null
    gWsjtUdpSocketReady = false
  }
  if (gClosing === true) {
    return
  }
  gWsjtUdpSocketError = false
  const dgram = require('dgram')
  gWsjtUdpServer = dgram.createSocket({
    type: 'udp4',
    reuseAddr: true
  })
  if (multicastEnable.checked === true && gAppSettings.wsjtIP !== '') {
    gWsjtUdpServer.on('listening', function () {
      const address = gWsjtUdpServer.address()
      gWsjtUdpServer.setBroadcast(true)
      gWsjtUdpServer.setMulticastTTL(128)
      gWsjtUdpServer.addMembership(gAppSettings.wsjtIP)
      gWsjtUdpSocketReady = true
    })
  } else {
    gAppSettings.multicast = false
    gWsjtCurrentIP = gAppSettings.wsjtIP = ''
    gWsjtUdpServer.on('listening', function () {
      gWsjtUdpServer.setBroadcast(true)
      gWsjtUdpSocketReady = true
    })
  }
  gWsjtUdpServer.on('error', function () {
    gWsjtUdpServer.close()
    gWsjtUdpServer = null
    gWsjtUdpSocketReady = false
    gWsjtUdpSocketError = true
  })
  gWsjtUdpServer.on('message', function (message, remote) {
    // if (gClosing == true) true;

    if (
      typeof udpForwardEnable !== 'undefined' &&
      udpForwardEnable.checked === true
    ) {
      sendForwardUdpMessage(
        message,
        message.length,
        udpForwardPortInput.value,
        udpForwardIpInput.value
      )
    }

    const newMessage = {}
    newMessage.magic_key = decodeQUINT32(message)
    message = message.slice(gQtToSplice)
    if (newMessage.magic_key === 0xadbccbda) {
      newMessage.schema_number = decodeQUINT32(message)
      message = message.slice(gQtToSplice)
      newMessage.type = decodeQUINT32(message)
      message = message.slice(gQtToSplice)
      newMessage.Id = decodeQUTF8(message)
      message = message.slice(gQtToSplice)

      const instanceId = newMessage.Id
      if (!(instanceId in gInstances)) {
        gInstances[toString(instanceId)] = {}
        gInstances[toString(instanceId)].valid = false
        gInstancesIndex.push(instanceId)
        gInstances[toString(instanceId)].intId = gInstancesIndex.length - 1
        gInstances[toString(instanceId)].crEnable = true
        if (gInstancesIndex.length > 1) {
          multiRigCRDiv.style.display = 'inline-block'
          haltTXDiv.style.display = 'inline-block'
        }
        updateRosterInstances()
      }
      let notify = false
      if (gInstances[toString(instanceId)].open === false) {
        notify = true
      }
      gInstances[toString(instanceId)].open = true
      gInstances[toString(instanceId)].remote = remote

      if (notify) {
        updateRosterInstances()
      }

      if (newMessage.type === 1) {
        newMessage.event = 'Status'
        newMessage.Frequency = decodeQUINT64(message)
        newMessage.Band = Number(newMessage.Frequency / 1000000).formatBand()
        message = message.slice(gQtToSplice)
        newMessage.MO = decodeQUTF8(message)
        message = message.slice(gQtToSplice)
        newMessage.DXcall = decodeQUTF8(message)
        message = message.slice(gQtToSplice)
        newMessage.Report = decodeQUTF8(message)
        message = message.slice(gQtToSplice)
        newMessage.TxMode = decodeQUTF8(message)
        message = message.slice(gQtToSplice)
        newMessage.TxEnabled = decodeQUINT8(message)
        message = message.slice(gQtToSplice)
        newMessage.Transmitting = decodeQUINT8(message)
        message = message.slice(gQtToSplice)
        newMessage.Decoding = decodeQUINT8(message)
        message = message.slice(gQtToSplice)
        newMessage.RxDF = decodeQINT32(message)
        message = message.slice(gQtToSplice)
        newMessage.TxDF = decodeQINT32(message)
        message = message.slice(gQtToSplice)
        newMessage.DEcall = decodeQUTF8(message)
        message = message.slice(gQtToSplice)
        newMessage.DEgrid = decodeQUTF8(message)
        message = message.slice(gQtToSplice)
        newMessage.DXgrid = decodeQUTF8(message)
        message = message.slice(gQtToSplice)
        newMessage.TxWatchdog = decodeQUINT8(message)
        message = message.slice(gQtToSplice)
        newMessage.Submode = decodeQUTF8(message)
        message = message.slice(gQtToSplice)
        newMessage.Fastmode = decodeQUINT8(message)
        message = message.slice(gQtToSplice)

        if (message.length > 0) {
          newMessage.SopMode = decodeQUINT8(message)
          message = message.slice(gQtToSplice)
        } else {
          newMessage.SopMode = -1
        }
        if (message.length > 0) {
          newMessage.FreqTol = decodeQINT32(message)
          message = message.slice(gQtToSplice)
        } else {
          newMessage.FreqTol = -1
        }
        if (message.length > 0) {
          newMessage.TRP = decodeQINT32(message)
          message = message.slice(gQtToSplice)
        } else {
          newMessage.TRP = -1
        }
        if (message.length > 0) {
          newMessage.ConfName = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
        } else {
          newMessage.ConfName = null
        }

        gInstances[toString(instanceId)].status = newMessage
        gInstances[toString(instanceId)].valid = true
      }
      if (gInstances[toString(instanceId)].valid === true) {
        if (newMessage.type === 2) {
          newMessage.event = 'Decode'
          newMessage.NW = decodeQUINT8(message)
          message = message.slice(gQtToSplice)
          newMessage.TM = decodeQUINT32(message)
          message = message.slice(gQtToSplice)
          newMessage.SR = decodeQINT32(message)
          message = message.slice(gQtToSplice)
          newMessage.DT = decodeQDOUBLE(message)
          message = message.slice(gQtToSplice)
          newMessage.DF = decodeQUINT32(message)
          message = message.slice(gQtToSplice)
          newMessage.MO = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.Msg = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.LC = decodeQUINT8(message)
          message = message.slice(gQtToSplice)
          newMessage.OA = decodeQUINT8(message)
          message = message.slice(gQtToSplice)
          newMessage.OF = gInstances[toString(instanceId)].status.Frequency
          newMessage.OC = gInstances[toString(instanceId)].status.DEcall
          newMessage.OG = gInstances[toString(instanceId)].status.DEgrid
          newMessage.OM = gInstances[toString(instanceId)].status.MO
          newMessage.OB = gInstances[toString(instanceId)].status.Band
        }
        if (newMessage.type === 3) {
          newMessage.event = 'Clear'
        }
        if (newMessage.type === 5) {
          newMessage.event = 'QSO Logged'
          newMessage.DateOff = decodeQUINT64(message)
          message = message.slice(gQtToSplice)
          newMessage.TimeOff = decodeQUINT32(message)
          message = message.slice(gQtToSplice)
          newMessage.timespecOff = decodeQUINT8(message)
          message = message.slice(gQtToSplice)
          if (newMessage.timespecOff === 2) {
            newMessage.offsetOff = decodeQINT32(message)
            message = message.slice(gQtToSplice)
          }
          newMessage.DXCall = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.DXGrid = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.Frequency = decodeQUINT64(message)
          message = message.slice(gQtToSplice)
          newMessage.MO = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.ReportSend = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.ReportRecieved = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.TXPower = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.Comments = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.Name = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.DateOn = decodeQUINT64(message)
          message = message.slice(gQtToSplice)
          newMessage.TimeOn = decodeQUINT32(message)
          message = message.slice(gQtToSplice)
          newMessage.timespecOn = decodeQUINT8(message)
          message = message.slice(gQtToSplice)
          if (newMessage.timespecOn === 2) {
            newMessage.offsetOn = decodeQINT32(message)
            message = message.slice(gQtToSplice)
          }
          if (message.length > 0) {
            newMessage.Operatorcall = decodeQUTF8(message)
            message = message.slice(gQtToSplice)
          } else {
            newMessage.Operatorcall = ''
          }

          if (message.length > 0) {
            newMessage.Mycall = decodeQUTF8(message)
            message = message.slice(gQtToSplice)
          } else {
            newMessage.Mycall = ''
          }

          if (message.length > 0) {
            newMessage.Mygrid = decodeQUTF8(message)
            message = message.slice(gQtToSplice)
          } else {
            newMessage.Mygrid = ''
          }

          if (message.length > 0) {
            newMessage.ExchangeSent = decodeQUTF8(message)
            message = message.slice(gQtToSplice)
          } else {
            newMessage.ExchangeSent = ''
          }

          if (message.length > 0) {
            newMessage.ExchangeReceived = decodeQUTF8(message)
            message = message.slice(gQtToSplice)
          } else {
            newMessage.ExchangeReceived = ''
          }
        }
        if (newMessage.type === 6) {
          newMessage.event = 'Close'
        }
        if (newMessage.type === 10) {
          newMessage.event = 'WSPRDecode'
          newMessage.NW = decodeQUINT8(message)
          message = message.slice(gQtToSplice)
          newMessage.TM = decodeQUINT32(message)
          message = message.slice(gQtToSplice)
          newMessage.SR = decodeQINT32(message)
          message = message.slice(gQtToSplice)
          newMessage.DT = decodeQDOUBLE(message)
          message = message.slice(gQtToSplice)
          newMessage.Frequency = decodeQUINT64(message)
          message = message.slice(gQtToSplice)
          newMessage.Drift = decodeQINT32(message)
          message = message.slice(gQtToSplice)
          newMessage.Callsign = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.Grid = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
          newMessage.Power = decodeQINT32(message)
          message = message.slice(gQtToSplice)
          newMessage.OA = decodeQUINT8(message)
          message = message.slice(gQtToSplice)
          newMessage.OF = gInstances[toString(instanceId)].status.Frequency
          newMessage.OC = gInstances[toString(instanceId)].status.DEcall
          newMessage.OG = gInstances[toString(instanceId)].status.DEgrid
          newMessage.OM = gInstances[toString(instanceId)].status.MO
          newMessage.OB = gInstances[toString(instanceId)].status.Band
        }
        if (newMessage.type === 12) {
          newMessage.event = 'ADIF'
          newMessage.ADIF = decodeQUTF8(message)
          message = message.slice(gQtToSplice)
        }

        if (newMessage.type in gWsjtHandlers) {
          newMessage.remote = remote
          newMessage.instance = instanceId

          lastMsgTimeDiv.innerHTML = 'Received from ' + newMessage.Id

          gWsjtHandlers[newMessage.type](newMessage)
          gLastTimeSinceMessageInSeconds = parseInt(Date.now() / 1000)
        }
      }
    }
  })
  gWsjtUdpServer.bind(port)
  gWsjtCurrentPort = port
  gWsjtCurrentIP = gAppSettings.wsjtIP
}
