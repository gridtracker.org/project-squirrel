function myCallCompare (a, b) {
  return a.DEcall.localeCompare(b.DEcall)
}

function myGridCompare (a, b) {
  return a.grid.localeCompare(b.grid)
}

function myModeCompare (a, b) {
  return a.mode.localeCompare(b.mode)
}

function myDxccCompare (a, b) {
  return gDxccToAltName[a.dxcc].localeCompare(gDxccToAltName[b.dxcc])
}

function myDxccIntCompare (a, b) {
  if (!(a in gDxccToAltName)) {
    return 0
  }
  if (!(b in gDxccToAltName)) {
    return gDxccToAltName[a].localeCompare(gDxccToAltName[b])
  }
}

function myTimeCompare (a, b) {
  if (a.time > b.time) {
    return 1
  }
  if (a.time < b.time) {
    return -1
  }
  return 0
}

function myBandCompare (a, b) {
  return a.band.localeCompare(b.band)
}

function myConfirmedCompare (a, b) {
  if (a.confirmed && !b.confirmed) {
    return 1
  }
  if (!a.confirmed && b.confirmed) {
    return -1
  }
  return 0
}

function numberSort (a, b) {
  // cut off 'm' from 80m or 70cm
  let metersA = a.slice(0, -1)
  let metersB = b.slice(0, -1)

  // if last letter is c we have a centimeter band, multiply value with 0.01
  if (metersA.slice(-1) === 'c') {
    metersA = 0.01 * parseInt(metersA)
  } else {
    metersA = parseInt(metersA)
  }
  if (metersB.slice(-1) === 'c') {
    metersB = 0.01 * parseInt(metersB)
  } else {
    metersA = parseInt(metersA)
  }
  if (metersA > metersB) {
    return 1
  }
  if (metersB > metersA) {
    return -1
  }
  return 0
}

function byName (a, b) {
  if (gEnums[a] < gEnums[b]) {
    return -1
  }
  if (gEnums[a] > gEnums[b]) {
    return 1
  }
  return 0
}
