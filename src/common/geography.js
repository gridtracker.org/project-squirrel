function twoWideToLatLong (qth) {
  qth = qth.toUpperCase()
  const a = qth.charCodeAt(0) - 65
  const b = qth.charCodeAt(1) - 65

  let la1 = b * 10
  let lo1 = a * 20
  let la2 = la1 + 10
  let lo2 = lo1 + 20
  const LatLong = []

  la1 -= 90
  lo1 -= 180
  la2 -= 90
  lo2 -= 180
  LatLong.la1 = la1
  LatLong.lo1 = lo1
  LatLong.la2 = la2
  LatLong.lo2 = lo2

  return LatLong
}

function squareToLatLongAll (qth) {
  qth = qth.toUpperCase()
  const a = qth.charCodeAt(0) - 65
  const b = qth.charCodeAt(1) - 65
  const c = qth.charCodeAt(2) - 48
  const d = qth.charCodeAt(3) - 48
  let la1 = b * 10 + d
  let lo1 = a * 20 + c * 2
  let la2
  let lo2
  const LatLong = []
  if (qth.length === 4) {
    la2 = la1 + 1
    lo2 = lo1 + 2
    LatLong.size = 4
  } else {
    let lo3
    let la3
    const e = qth.charCodeAt(4) - 65
    const f = qth.charCodeAt(5) - 65
    const R = 5 / 60
    const T = 2.5 / 60
    lo3 = (e * 5) / 60
    la3 = (f * 2.5) / 60
    la1 += la3
    lo1 += lo3
    la2 = la1 + T
    lo2 = lo1 + R
    LatLong.size = 6
  }

  la1 -= 90
  lo1 -= 180
  la2 -= 90
  lo2 -= 180
  LatLong.la1 = la1
  LatLong.lo1 = lo1
  LatLong.la2 = la2
  LatLong.lo2 = lo2

  return LatLong
}

function squareToLatLong (qth) {
  qth = qth.toUpperCase()
  const a = qth.charCodeAt(0) - 65
  const b = qth.charCodeAt(1) - 65
  const c = qth.charCodeAt(2) - 48
  const d = qth.charCodeAt(3) - 48
  let la1 = b * 10 + d
  let lo1 = a * 20 + c * 2
  let la2
  let lo2
  const LatLong = []
  if (qth.length === 4 || gAppSettings.sixWideMode === 0) {
    la2 = la1 + 1
    lo2 = lo1 + 2
    LatLong.size = 4
  } else {
    let lo3
    let la3
    const e = qth.charCodeAt(4) - 65
    const f = qth.charCodeAt(5) - 65
    const R = 5 / 60
    const T = 2.5 / 60
    lo3 = (e * 5) / 60
    la3 = (f * 2.5) / 60
    la1 += la3
    lo1 += lo3
    la2 = la1 + T
    lo2 = lo1 + R
    LatLong.size = 6
  }
  la1 -= 90
  lo1 -= 180
  la2 -= 90
  lo2 -= 180
  LatLong.la1 = la1
  LatLong.lo1 = lo1
  LatLong.la2 = la2
  LatLong.lo2 = lo2

  return LatLong
}

function lineString (points) {
  const thing = new ol.geom.LineString(points)
  const rect = new ol.Feature({ geometry: thing })
  return rect
}

function rectangle (bounds, options) {
  const thing = new ol.geom.Polygon([
    [
      ol.proj.fromLonLat([bounds[0][0], bounds[0][1]]),
      ol.proj.fromLonLat([bounds[0][0], bounds[1][1]]),
      ol.proj.fromLonLat([bounds[1][0], bounds[1][1]]),
      ol.proj.fromLonLat([bounds[1][0], bounds[0][1]])
    ]
  ])
  const rect = new ol.Feature({
    name: 'rect',
    geometry: thing
  })
  return rect
}

function triangle (bounds, topLeft) {
  let thing = null

  if (topLeft) {
    thing = new ol.geom.Polygon([
      [
        ol.proj.fromLonLat([bounds[0][0], bounds[0][1]]),
        ol.proj.fromLonLat([bounds[0][0], bounds[1][1]]),
        ol.proj.fromLonLat([bounds[1][0], bounds[1][1]]),
        ol.proj.fromLonLat([bounds[0][0], bounds[0][1]])
      ]
    ])
  } else {
    thing = new ol.geom.Polygon([
      [
        ol.proj.fromLonLat([bounds[0][0], bounds[0][1]]),
        ol.proj.fromLonLat([bounds[1][0], bounds[1][1]]),
        ol.proj.fromLonLat([bounds[1][0], bounds[0][1]]),
        ol.proj.fromLonLat([bounds[0][0], bounds[0][1]])
      ]
    ])
  }

  const rect = new ol.Feature({
    name: 'rect',
    geometry: thing
  })
  return rect
}

function triangleToGrid (iQTH, feature) {
  const LL = squareToLatLong(iQTH)
  const bounds = [
    [LL.lo1, LL.la1],
    [LL.lo2, LL.la2]
  ]

  const thing = new ol.geom.Polygon([
    [
      ol.proj.fromLonLat([bounds[0][0], bounds[0][1]]),
      ol.proj.fromLonLat([bounds[0][0], bounds[1][1]]),
      ol.proj.fromLonLat([bounds[1][0], bounds[1][1]]),
      ol.proj.fromLonLat([bounds[1][0], bounds[0][1]]),
      ol.proj.fromLonLat([bounds[0][0], bounds[0][1]])
    ]
  ])

  feature.setGeometry(thing)
}

function gridToTriangle (iQTH, feature, topLeft) {
  const LL = squareToLatLong(iQTH)
  const bounds = [
    [LL.lo1, LL.la1],
    [LL.lo2, LL.la2]
  ]
  let thing = null

  if (topLeft) {
    thing = new ol.geom.Polygon([
      [
        ol.proj.fromLonLat([bounds[0][0], bounds[0][1]]),
        ol.proj.fromLonLat([bounds[0][0], bounds[1][1]]),
        ol.proj.fromLonLat([bounds[1][0], bounds[1][1]]),
        ol.proj.fromLonLat([bounds[0][0], bounds[0][1]])
      ]
    ])
  } else {
    thing = new ol.geom.Polygon([
      [
        ol.proj.fromLonLat([bounds[0][0], bounds[0][1]]),
        ol.proj.fromLonLat([bounds[1][0], bounds[1][1]]),
        ol.proj.fromLonLat([bounds[1][0], bounds[0][1]]),
        ol.proj.fromLonLat([bounds[0][0], bounds[0][1]])
      ]
    ])
  }

  feature.setGeometry(thing)
}

function getPoint (grid) {
  const LL = squareToLatLongAll(grid)
  const Lat = LL.la2 - (LL.la2 - LL.la1) / 2
  const Lon = LL.lo2 - (LL.lo2 - LL.lo1) / 2
  return ol.proj.fromLonLat([Lon, Lat])
}

function shapeFeature (
  key,
  geoJsonData,
  propname,
  fillColor,
  borderColor,
  borderWidth
) {
  const feature = new ol.format.GeoJSON({
    geometryName: key
  }).readFeature(geoJsonData, {
    featureProjection: 'EPSG:3857'
  })

  const style = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: borderColor,
      width: borderWidth
    }),
    fill: new ol.style.Fill({
      color: fillColor
    })
  })

  feature.setStyle(style)
  feature.set('prop', propname)
  feature.size = 2
  return feature
}

function centerOn (grid) {
  if (grid.length >= 4) {
    const LL = squareToLatLong(grid)
    gMap
      .getView()
      .setCenter(
        ol.proj.fromLonLat([
          LL.lo2 - (LL.lo2 - LL.lo1) / 2,
          LL.la2 - (LL.la2 - LL.la1) / 2
        ])
      )
  }
}

function setCenterQTH () {
  if (homeQTHInput.value.length >= 4) {
    gAppSettings.centerGridsquare = homeQTHInput.value
    // Grab home QTH Gridsquare from Center QTH
    const LL = squareToLatLong(homeQTHInput.value)

    // panTo(ol.proj.fromLonLat([LL.lo2 - (LL.lo2 - LL.lo1) / 2, LL.la2 - ((LL.la2 - LL.la1) / 2)]));
    gMap
      .getView()
      .setCenter(
        ol.proj.fromLonLat([
          LL.lo2 - (LL.lo2 - LL.lo1) / 2,
          LL.la2 - (LL.la2 - LL.la1) / 2
        ])
      )
  } else {
    homeQTHInput.value = ''
  }
}

function setCenterGridsquare () {
  if (gMapMemory[6].zoom !== -1) {
    mapMemory(6, false)
    return
  }
  setCenterQTH()
}

function setGridMode (mode) {
  gAppSettings.sixWideMode = mode
  modeImg.src = gMaidenheadModeImageArray[gAppSettings.sixWideMode]
  clearTempGrids()
  redrawGrids()
}

function toggleGridMode () {
  gAppSettings.sixWideMode ^= 1
  modeImg.src = gMaidenheadModeImageArray[gAppSettings.sixWideMode]
  clearTempGrids()
  redrawGrids()
}

function newDistanceObject (start = 0) {
  const distance = {}
  distance.worked_unit = start
  distance.worked_hash = ''
  distance.confirmed_unit = start
  distance.confirmed_hash = null
  return distance
}

function ValidateGridsquareOnly4 (inputText, validDiv) {
  addError.innerHTML = ''
  if (inputText.value.length === 4) {
    let gridSquare = ''
    const LETTERS = inputText.value.substr(0, 2).toUpperCase()
    const NUMBERS = inputText.value.substr(2, 2).toUpperCase()
    if (/^[A-R]+$/.test(LETTERS) && /^[0-9]+$/.test(NUMBERS)) {
      gridSquare = LETTERS + NUMBERS
    }
    if (gridSquare !== '') {
      inputText.style.color = '#FF0'
      inputText.style.backgroundColor = 'green'
      inputText.value = gridSquare
      if (validDiv) {
        validDiv.innerHTML = 'Valid!'
      }
      return true
    } else {
      inputText.style.color = '#FFF'
      inputText.style.backgroundColor = 'red'
      if (validDiv) {
        validDiv.innerHTML = 'Invalid!'
      }
      return false
    }
  } else {
    inputText.style.color = '#000'
    inputText.style.backgroundColor = 'yellow'
    if (validDiv) {
      validDiv.innerHTML = 'Valid!'
    }
    return true
  }
}

function validateGridFromString (inputText) {
  if (inputText.length === 4 || inputText.length === 6) {
    let gridSquare = ''
    const LETTERS = inputText.substr(0, 2).toUpperCase()
    const NUMBERS = inputText.substr(2, 2).toUpperCase()
    if (/^[A-R]+$/.test(LETTERS) && /^[0-9]+$/.test(NUMBERS)) {
      gridSquare = LETTERS + NUMBERS
    }
    if (inputText.length > 4) {
      const LETTERS_SUB = inputText.substr(4, 2).toUpperCase()
      gridSquare = ''
      if (
        /^[A-R]+$/.test(LETTERS) &&
        /^[0-9]+$/.test(NUMBERS) &&
        /^[A-Xa-x]+$/.test(LETTERS_SUB)
      ) {
        gridSquare = LETTERS + NUMBERS + LETTERS_SUB
      }
    }
    if (gridSquare !== '') {
      return true
    } else {
      return false
    }
  } else {
    return false
  }
}

function textToGrid (inputText) {
  if (inputText.length === 4 || inputText.length === 6) {
    let gridSquare = ''
    const LETTERS = inputText.substr(0, 2).toUpperCase()
    const NUMBERS = inputText.substr(2, 2).toUpperCase()
    if (/^[A-R]+$/.test(LETTERS) && /^[0-9]+$/.test(NUMBERS)) {
      gridSquare = LETTERS + NUMBERS
    }
    if (inputText.length > 4) {
      const LETTERS_SUB = inputText.substr(4, 2).toUpperCase()
      gridSquare = ''
      if (
        /^[A-R]+$/.test(LETTERS) &&
        /^[0-9]+$/.test(NUMBERS) &&
        /^[A-Xa-x]+$/.test(LETTERS_SUB)
      ) {
        gridSquare = LETTERS + NUMBERS + LETTERS_SUB
      }
    }
    if (gridSquare !== '') {
      return inputText
    } else {
      return (false)
    }
  } else {
    return false
  }
}

function ValidateGridsquare (inputText, validDiv) {
  if (inputText.value.length === 4 || inputText.value.length === 6) {
    let gridSquare = ''
    const LETTERS = inputText.value.substr(0, 2).toUpperCase()
    const NUMBERS = inputText.value.substr(2, 2).toUpperCase()
    if (/^[A-R]+$/.test(LETTERS) && /^[0-9]+$/.test(NUMBERS)) {
      gridSquare = LETTERS + NUMBERS
    }
    if (inputText.value.length > 4) {
      const LETTERS_SUB = inputText.value.substr(4, 2)
      gridSquare = ''
      if (
        /^[A-R]+$/.test(LETTERS) &&
        /^[0-9]+$/.test(NUMBERS) &&
        /^[A-Xa-x]+$/.test(LETTERS_SUB)
      ) {
        gridSquare = LETTERS + NUMBERS + LETTERS_SUB
      }
    }
    if (gridSquare !== '') {
      inputText.style.color = '#FF0'
      inputText.style.backgroundColor = 'green'
      inputText.value = gridSquare
      if (validDiv) {
        validDiv.innerHTML = 'Valid!'
      }
      return true
    } else {
      inputText.style.color = '#FFF'
      inputText.style.backgroundColor = 'red'
      if (validDiv) {
        validDiv.innerHTML = 'Invalid!'
      }
      return false
    }
  } else {
    inputText.style.color = '#FFF'
    inputText.style.backgroundColor = 'red'
    if (validDiv) {
      validDiv.innerHTML = 'Invalid!'
    }
    return false
  }
}

function drawAllGrids () {
  const borderColor = '#000'
  let borderWeight = 0.5

  for (let x = -178; x < 181; x += 2) {
    const fromPoint = ol.proj.fromLonLat([x, -90])
    const toPoint = ol.proj.fromLonLat([x, 90])

    const points = [fromPoint, toPoint]

    if (x % 20 === 0) {
      borderWeight = 0.75
    } else {
      borderWeight = 0.25
    }

    const newGridBox = lineString(points)

    const featureStyle = new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: borderColor,
        width: borderWeight
      })
    })
    newGridBox.setStyle(featureStyle)

    gLayerSources['line-grids'].addFeature(newGridBox)
  }

  for (let x = -90; x < 91; x++) {
    const fromPoint = ol.proj.fromLonLat([-180, x])
    const toPoint = ol.proj.fromLonLat([180, x])

    const points = [fromPoint, toPoint]

    if (x % 10 === 0) {
      borderWeight = 0.75
    } else {
      borderWeight = 0.25
    }

    const newGridBox = lineString(points)

    const featureStyle = new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: borderColor,
        width: borderWeight
      })
    })
    newGridBox.setStyle(featureStyle)

    gLayerSources['line-grids'].addFeature(newGridBox)
  }

  for (let x = 65; x < 83; x++) {
    for (let y = 65; y < 83; y++) {
      for (let a = 0; a < 10; a++) {
        for (let b = 0; b < 10; b++) {
          const LL = squareToLatLong(
            String.fromCharCode(x) +
            String.fromCharCode(y) +
            String(a) +
            String(b)
          )
          const Lat = LL.la2 - (LL.la2 - LL.la1) / 2
          const Lon = LL.lo2 - (LL.lo2 - LL.lo1) / 2
          const point = ol.proj.fromLonLat([Lon, Lat])
          let feature = new ol.Feature({
            geometry: new ol.geom.Point(point),
            name: String(a) + String(b)
          })

          let featureStyle = new ol.style.Style({
            text: new ol.style.Text({
              fill: new ol.style.Fill({ color: '#000' }),
              font: 'normal 16px sans-serif',
              stroke: new ol.style.Stroke({
                color: '#88888888',
                width: 1
              }),
              text: String(a) + String(b),
              offsetY: 1
            })
          })
          feature.setStyle(featureStyle)
          gLayerSources['short-grids'].addFeature(feature)

          feature = new ol.Feature({
            geometry: new ol.geom.Point(point),
            name: String(a) + String(b)
          })

          featureStyle = new ol.style.Style({
            text: new ol.style.Text({
              fill: new ol.style.Fill({ color: '#000' }),
              font: 'normal 16px sans-serif',
              stroke: new ol.style.Stroke({
                color: '#88888888',
                width: 1
              }),
              text:
                String.fromCharCode(x) +
                String.fromCharCode(y) +
                String(a) +
                String(b),
              offsetY: 1
            })
          })
          feature.setStyle(featureStyle)
          gLayerSources['long-grids'].addFeature(feature)
        }
      }

      const LL = twoWideToLatLong(
        String.fromCharCode(x) + String.fromCharCode(y)
      )
      const Lat = LL.la2 - (LL.la2 - LL.la1) / 2
      const Lon = LL.lo2 - (LL.lo2 - LL.lo1) / 2
      const point = ol.proj.fromLonLat([Lon, Lat])
      feature = new ol.Feature(new ol.geom.Point(point))

      featureStyle = new ol.style.Style({
        text: new ol.style.Text({
          fill: new ol.style.Fill({ color: '#000' }),
          font: 'normal 24px sans-serif',
          stroke: new ol.style.Stroke({
            color: '#88888888',
            width: 1
          }),
          text: String.fromCharCode(x) + String.fromCharCode(y)
        })
      })
      feature.setStyle(featureStyle)
      gLayerSources['big-grids'].addFeature(feature)
    }
  }
}
