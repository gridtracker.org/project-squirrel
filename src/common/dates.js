function dateToString (dateTime) {
  if (gAppSettings.useLocalTime === 1) {
    return dateTime.toLocaleString().replace(/,/g, '')
  } else {
    return dateTime.toUTCString().replace(/GMT/g, 'UTC').replace(/,/g, '')
  }
}

function userDayString (Msec) {
  let dateTime
  if (Msec != null) {
    dateTime = new Date(Msec)
  } else {
    dateTime = new Date()
  }

  const ds = dateTime.toUTCString().replace(/GMT/g, 'UTC').replace(/,/g, '')
  const dra = ds.split(' ')
  dra.shift()
  dra.pop()
  dra.pop()
  return dra.join(' ')
}

function userTimeString (Msec) {
  let dateTime
  if (Msec != null) {
    dateTime = new Date(Msec)
  } else {
    dateTime = new Date()
    return dateToString(dateTime)
  }
}

function dateToISO8601 (dString, tZone) {
  let retDate = ''
  tZone = (typeof tZone !== 'undefined') ? tZone : 'Z'
  const dateParts = dString.match(/(\d{4}-\d{2}-\d{2})(\s+(\d{2}:\d{2}:\d{2}))?/)

  if (dateParts !== null) {
    retDate = dateParts[1]
    if ((typeof dateParts[3]) !== 'undefined') {
      retDate += 'T' + dateParts[3] + '.000' + tZone
    } else {
      retDate += 'T00:00:00.000' + tZone
    }
  }
  return retDate
}

function compareCallsignTime (a, b) {
  if (a.time < b.time) {
    return -1
  }
  if (a.time > b.time) {
    return 1
  }
  return 0
}

function timeNowSec () {
  return parseInt(Date.now() / 1000)
}
