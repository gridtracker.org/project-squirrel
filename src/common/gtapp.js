function checkForNewVersion (showUptoDate) {
  if (typeof nw !== 'undefined') {
    getBuffer(
      'https://storage.googleapis.com/gt_app/version.txt',
      versionCheck,
      showUptoDate,
      'https',
      443
    )
  }
}

function downloadAcknowledgements () {
  if (gMapSettings.offlineMode === false) {
    getBuffer(
      'https://storage.googleapis.com/gt_app/acknowledgements.json',
      updateAcks,
      null,
      'https',
      443
    )

    setTimeout(downloadAcknowledgements, 8640000)
  }
}

function versionCheck (buffer, flag) {
  const version = String(buffer)
  if (version.indexOf('gridtracker') == 0) {
    // good, we're looking at our version string
    const versionArray = version.split(':')
    if (versionArray.length == 3) {
      // Good, there are 3 parts
      const stableVersion = Number(versionArray[1])
      const betaVersion = Number(versionArray[2])

      if (gtVersion < stableVersion) {
        const verString = String(stableVersion)
        main.style.display = 'none'
        newVersionMustDownloadDiv.innerHTML =
          'New Version v' +
          verString.substr(0, 1) +
          '.' +
          verString.substr(1, 2) +
          '.' +
          verString.substr(3) +
          ' available for download.<br />Go there now?<br /><br />'
        versionDiv.style.display = 'block'
      } else {
        if (flag) {
          if (gtVersion < betaVersion) {
            const verString = String(betaVersion)
            main.style.display = 'none'
            newVersionMustDownloadDiv.innerHTML =
              'New <b><i>Beta</i></b> Version v' +
              verString.substr(0, 1) +
              '.' +
              verString.substr(1, 2) +
              '.' +
              verString.substr(3) +
              ' available for download.<br />Go there now?<br /><br />'
            versionDiv.style.display = 'block'
          } else {
            main.style.display = 'none'
            upToDateDiv.style.display = 'block'
          }
        }
      }
    }
  }
}

function updateAcks (buffer) {
  try {
    gAcknowledgedCalls = JSON.parse(buffer)
  } catch (e) {
    // can't write, somethings broke
  }
}

function readAcksFromDisk () {
  try {
    const fileBuf = fs.readFileSync('assets/data/acknowledgements.json')
    const loadedData = JSON.parse(fileBuf)
    // some validation here?
    gAcknowledgedCalls = loadedData
  } catch (e) {
    // file failed to load, probably not downloaded
  }
}
