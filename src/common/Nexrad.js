let gNexradInterval = null
let gNexradEnable = 0

function createNexRad () {
  const layerSource = new ol.source.TileWMS({
    url: 'http://mesonet.agron.iastate.edu/cgi-bin/wms/nexrad/n0q.cgi',
    params: { LAYERS: 'nexrad-n0q' }
  })

  const layerVector = new ol.layer.Tile({
    source: layerSource,
    visible: true,
    zIndex: 900
  })

  layerVector.set('name', 'radar')

  return layerVector
}

function toggleNexrad () {
  gNexradEnable ^= 1
  if (gNexradEnable === 1) {
    if (gNexrad != null) {
      gMap.removeLayer(gNexrad)
    }

    gNexrad = createNexRad()
    gMap.addLayer(gNexrad)

    if (gNexradInterval === null) {
      gNexradInterval = setInterval(nexradRefresh, 600000)
    }
  } else {
    if (gNexradInterval !== null) {
      clearInterval(gNexradInterval)
      gNexradInterval = null
    }
    if (gNexrad) {
      gMap.removeLayer(gNexrad)
      gNexrad = null
    }
  }

  gMapSettings.usNexrad = gNexradEnable === 1
}

function nexradRefresh () {
  if (gNexrad != null) {
    gNexrad.getSource().refresh()
  }
}
