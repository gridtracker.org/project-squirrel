function getBuffer (file_url, callback, flag, mode, port, cache = null) {
  const url = require('url')
  const http = require(mode)
  let fileBuffer = null
  let options = null

  options = {
    host: url.parse(file_url).host, // eslint-disable-line node/no-deprecated-api
    port,
    followAllRedirects: true,
    path: url.parse(file_url).path, // eslint-disable-line node/no-deprecated-api
    headers: { 'User-Agent': gtVersionString }
  }

  http.get(options, function (res) {
    const fsize = res.headers['content-length']
    let cookies = null
    if (typeof res.headers['set-cookie'] !== 'undefined') {
      cookies = res.headers['set-cookie']
    }
    res
      .on('data', function (data) {
        if (fileBuffer === null) {
          fileBuffer = data
        } else {
          fileBuffer += data
        }
      })
      .on('end', function () {
        if (typeof callback === 'function') {
          // Call it, since we have confirmed it is callable
          callback(fileBuffer, flag, cache)
        }
      })
      .on('error', function (e) {
        console.error('getBuffer ' + file_url + ' error: ' + e.message)
      })
  })
}

function getPostBuffer (
  file_url,
  callback,
  flag,
  mode,
  port,
  theData,
  timeoutMs,
  timeoutCallback,
  who
) {
  const querystring = require('querystring')
  const postData = querystring.stringify(theData)
  const url = require('url')
  const http = require(mode)
  let fileBuffer = null
  const options = {
    host: url.parse(file_url).host, // eslint-disable-line node/no-deprecated-api
    port,
    path: url.parse(file_url).path, // eslint-disable-line node/no-deprecated-api
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': postData.length
    }
  }
  const req = http.request(options, function (res) {
    const fsize = res.headers['content-length']
    let cookies = null
    if (typeof res.headers['set-cookie'] !== 'undefined') {
      cookies = res.headers['set-cookie']
    }
    res
      .on('data', function (data) {
        if (fileBuffer == null) fileBuffer = data
        else fileBuffer += data
      })
      .on('end', function () {
        if (typeof callback === 'function') {
          // Call it, since we have confirmed it is callable
          callback(fileBuffer, flag, cookies)
        }
      })
      .on('error', function () {
        if (typeof errorCallback === 'function') {
          errorCallback()
        }
      })
  })
  if (typeof timeoutMs === 'number' && timeoutMs > 0) {
    req.on('socket', function (socket) {
      socket.setTimeout(timeoutMs)
      socket.on('timeout', function () {
        req.abort()
      })
    })
    req.on('error', function (err) { // eslint-disable-line node/handle-callback-err
      if (typeof timeoutCallback !== 'undefined') {
        timeoutCallback(
          file_url,
          callback,
          flag,
          mode,
          port,
          theData,
          timeoutMs,
          timeoutCallback,
          who
        )
      }
    })
  }
  req.write(postData)
  req.end()
}
