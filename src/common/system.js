function startupEventsAndTimers () {
  document.addEventListener('keydown', onMyKeyDown, true)
  document.addEventListener('keyup', onMyKeyUp, false)
  displayTimeInterval = setInterval(displayTime, 1000)
}

const gFinishedLoading = false
function postInit () {
  redrawSpots()
  checkForSettings()
  updateForwardListener()
  addLastTraffic('GrsidTracker</br>' + gtShortVersion)

  gNexradEnable = gMapSettings.usNexrad ? 0 : 1
  toggleNexrad()

  if (String(gtVersion) != String(gStartVersion)) {
    // generalbut.className = "settingsTablinks";
    showSettingsBox()

    openSettingsTab(updatebut, 'updateSettingsDiv')
  }
  gFinishedLoading = true
  // tagme
  const x = document.querySelectorAll("input[type='range']")
  for (let i = 0; i < x.length; i++) {
    if (x[i].title.length > 0) {
      x[i].title += '\n'
    }
    x[i].title += '(Use Arrow Keys For Smaller Increments)'
  }

  openLookupWindow(false)
  openBaWindow(false)
  openCallRosterWindow(false)
  openConditionsWindow(false)
  showMessaging(false)
}

document.addEventListener('dragover', function (event) {
  event.preventDefault()
})

document.addEventListener('drop', function (event) {
  event.preventDefault()
  if (gFinishedLoading === true) {
    dropHandler(event)
  }
})

const gStartupTable = [
  [startupVersionInit, 'Completed Version Check'],
  [qsoBackupFileInit, 'QSO Backup Initialized'],
  [callsignServicesInit, 'Callsign Services Initialized'],
  [loadMapSettings, 'Map Settings Initialized'],
  [initMap, 'Loaded Map'],
  [setPins, 'Created Pins'],
  [loadViewSettings, 'Loaded View Settings'],
  [loadMsgSettings, 'Loaded Messaging Settings'],
  [setFileSelectors, 'Set File Selectors'],
  [lockNewWindows, 'Locked New Windows'],
  [loadMaidenHeadData, 'Loaded Maidenhead Dataset'],
  [drawAllGrids, 'Rendered All Maidenhead Grids'],
  [updateRunningProcesses, 'Updated Running Processes'],
  [updateBasedOnIni, 'Updated from WSJT-X/JTDX'],
  [loadAdifSettings, 'Loaded ADIF Settings'],
  [startupButtonsAndInputs, 'Buttons and Inputs Initialized'],
  [initSpeech, 'Speech Initialized'],
  [initSoundCards, 'Sounds Initialized'],
  [loadPortSettings, 'Loaded Network Settings'],
  [loadLookupDetails, 'Callsign Lookup Details Loaded'],
  [startupEventsAndTimers, 'Set Events and Timers'],
  [registerHotKeys, 'Registered Hotkeys'],
  [gtChatSystemInit, 'Chat System Initialized'],
  [getPotaPlaces, 'Loading POTA Database'],
  [getPotaSpots, 'Starting POTA Spots Pump'],
  [downloadAcknowledgements, 'Contributor Acknowledgements Loaded'],
  [postInit, 'Finalizing System']
]

function init () {
  startupVersionDiv.innerHTML = gtVersionString
  aboutVersionDiv.innerHTML = gtVersionString
  gCurrentDay = parseInt(timeNowSec() / 86400)
  if (mediaCheck() === false) {
    startupDiv.style.display = 'none'
    documentsDiv.style.display = 'block'
    searchedDocFolder.innerHTML = gAppData
  } else {
    documentsDiv.style.display = 'none'
    startupDiv.style.display = 'block'
    startupStatusDiv.innerHTML = 'Starting...'
    setTimeout(startupEngine, 10)
  }
}

function startupEngine () {
  if (gStartupTable.length > 0) {
    const funcInfo = gStartupTable.shift()
    funcInfo[0]()
    startupStatusDiv.innerHTML = funcInfo[1]
    setTimeout(startupEngine, 10)
  } else {
    startupStatusDiv.innerHTML = 'Completed'
    setTimeout(endStartup, 2000)
    startupAdifLoadCheck()
    openStatsWindow(false)
  }
}

function directoryInput (what) {
  gAppSettings.savedAppData = what.files[0].path
  init()
}

function endStartup () {
  startupDiv.style.display = 'none'
  main.style.display = 'block'
  gMap.updateSize()
}

let gQtToSplice = 0

function decodeQUINT8 (byteArray) {
  gQtToSplice = 1
  return byteArray[0]
}

function encodeQBOOL (byteArray, offset, value) {
  return byteArray.writeUInt8(value, offset)
}

function decodeQUINT32 (byteArray) {
  gQtToSplice = 4
  return byteArray.readUInt32BE(0)
}

function encodeQUINT32 (byteArray, offset, value) {
  if (value === -1) {
    value = 4294967295
  }
  return byteArray.writeUInt32BE(value, offset)
}

function decodeQINT32 (byteArray) {
  gQtToSplice = 4
  return byteArray.readInt32BE(0)
}

function encodeQINT32 (byteArray, offset, value) {
  return byteArray.writeInt32BE(value, offset)
}

function decodeQUINT64 (byteArray) {
  let value = 0
  for (let i = 0; i < 8; i++) {
    value = value * 256 + byteArray[i]
  }
  gQtToSplice = 8
  return value
}

function encodeQUINT64 (byteArray, offset, value) {
  const breakOut = Array()
  for (let i = 0; i < 8; i++) {
    breakOut[i] = value & 0xff
    value >>= 8
  }
  for (let i = 0; i < 8; i++) {
    offset = encodeQBOOL(byteArray, offset, breakOut[7 - i])
  }
  return offset
}

function decodeQUTF8 (byteArray) {
  let utf8Len = decodeQUINT32(byteArray)
  let result = ''
  byteArray = byteArray.slice(gQtToSplice)
  if (utf8Len === 0xffffffff) {
    utf8Len = 0
  } else {
    result = byteArray.slice(0, utf8Len)
  }
  gQtToSplice = utf8Len + 4
  return result.toString()
}

function encodeQUTF8 (byteArray, offset, value) {
  offset = encodeQUINT32(byteArray, offset, value.length)
  const wrote = byteArray.write(value, offset, value.length)
  return wrote + offset
}

function decodeQDOUBLE (byteArray) {
  gQtToSplice = 8
  return byteArray.readDoubleBE(0)
}

function encodeQDOUBLE (byteArray, offset, value) {
  return byteArray.writeDoubleBE(value, offset)
}
