function getWpx (callsign) {
  let prefix = null

  if (callsign.includes('/')) {
    // Handle in the future?
    return null
  }
  if (!/\d/.test(callsign)) {
    // Insert 0, never seen this
    return null
  }

  const end = callsign.length
  let foundPrefix = false
  let prefixEnd = 1
  while (prefixEnd !== end) {
    if (/\d/.test(callsign.charAt(prefixEnd))) {
      while (prefixEnd + 1 !== end && /\d/.test(callsign.charAt(prefixEnd + 1))) {
        prefixEnd++
      }
      foundPrefix = true
      break
    }
    prefixEnd++
  }

  if (foundPrefix) {
    prefix = callsign.substr(0, prefixEnd + 1)
  }
  return prefix
}

function setState (details) {
  if (details.state !== null && details.state.length > 0) {
    const isDigi = details.digital

    if (details.state.substr(0, 2) !== 'US') {
      details.state = 'US-' + details.state
    }

    gTracker.worked.state[details.state + details.band + details.mode] = true
    gTracker.worked.state[details.state] = true
    gTracker.worked.state[details.state + details.mode] = true
    gTracker.worked.state[details.state + details.band] = true
    if (isDigi) {
      gTracker.worked.state[details.state + 'dg'] = true
      gTracker.worked.state[details.state + details.band + 'dg'] = true
    }

    if (details.confirmed) {
      gTracker.confirmed.state[
        details.state + details.band + details.mode
      ] = true
      gTracker.confirmed.state[details.state] = true
      gTracker.confirmed.state[details.state + details.mode] = true
      gTracker.confirmed.state[details.state + details.band] = true
      if (isDigi) {
        gTracker.confirmed.state[details.state + 'dg'] = true
        gTracker.confirmed.state[details.state + details.band + 'dg'] = true
      }
    }
  }

  if (details.cnty !== +null && details.cnty.length > 0) {
    const isDigi = details.digital

    gTracker.worked.cnty[details.cnty + details.band + details.mode] = true
    gTracker.worked.cnty[details.cnty] = true
    gTracker.worked.cnty[details.cnty + details.mode] = true
    gTracker.worked.cnty[details.cnty + details.band] = true
    if (isDigi) {
      gTracker.worked.cnty[details.cnty + 'dg'] = true
      gTracker.worked.cnty[details.cnty + details.band + 'dg'] = true
    }

    if (details.confirmed) {
      gTracker.confirmed.cnty[
        details.cnty + details.band + details.mode
      ] = true
      gTracker.confirmed.cnty[details.cnty] = true
      gTracker.confirmed.cnty[details.cnty + details.mode] = true
      gTracker.confirmed.cnty[details.cnty + details.band] = true
      if (isDigi) {
        gTracker.confirmed.cnty[details.cnty + 'dg'] = true
        gTracker.confirmed.cnty[details.cnty + details.band + 'dg'] = true
      }
    }
  }
}

function isKnownCallsignDXCC (dxcc) {
  if (dxcc in gCallsignDatabaseDXCC) {
    return true
  }
  return false
}

function isKnownCallsignUS (dxcc) {
  if (dxcc in gCallsignDatabaseUS) {
    return true
  }
  return false
}

function isKnownCallsignUSplus (dxcc) {
  if (dxcc in gCallsignDatabaseUSplus) {
    return true
  }
  return false
}

function addDeDx (
  finalGrid,
  finalDXcall,
  cq,
  cqdx,
  locked,
  finalDEcall,
  finalRSTsent,
  finalTime,
  ifinalMsg,
  mode,
  band,
  confirmed,
  notQso,
  finalRSTrecv,
  finalDxcc,
  finalState,
  finalCont,
  finalCnty,
  finalCqZone,
  finalItuZone,
  finalVucc = [],
  finalPropMode = '',
  finalDigital = false,
  finalPhone = false,
  finalIOTA = '',
  finalSatName = ''
) {
  const currentYear = new Date().getFullYear()
  const qsoDate = new Date(1970, 0, 1); qsoDate.setSeconds(finalTime)
  const isCurrentYear = (qsoDate.getFullYear() === currentYear)

  let callsign = null
  let rect = null
  let worked = false
  let didConfirm = false
  const wspr = mode === 'WSPR' ? parseInt(band) * 2 : null
  let hash = ''

  let finalMsg = ifinalMsg.trim()
  if (finalMsg.length > 40) {
    finalMsg = finalMsg.substring(0, 40) + '...'
  }
  let details = null
  if (!notQso) {
    const timeMod = finalTime - (finalTime % 360) + 180
    hash = unique(mode + band + finalDXcall + timeMod)

    let lookupCall = false

    if (hash in gQSOHash) {
      details = gQSOHash[toString(hash)]
      if (finalGrid.length > 0 && finalGrid !== details.grid) {
        // only touch the grid if it's larger than the last grid && the 4wide is the same
        if (details.grid.length < 6 && (details.grid.substr(0, 4) === finalGrid.substr(0, 4) || details.grid.length === 0)) {
          details.grid = finalGrid
          details.grid4 = finalGrid.substr(0, 4)
        }
      }
      if (finalRSTsent.length > 0) details.RSTsent = finalRSTsent
      if (finalRSTrecv.length > 0) details.RSTrecv = finalRSTrecv
      if (finalCqZone.length > 0) details.cqz = finalCqZone
      if (finalItuZone.length > 0) details.ituz = finalItuZone
      if (finalState !== null) details.state = finalState
      if (finalState === null && details.state) finalState = details.state
      if (finalDxcc < 1 && details.dxcc > 0) finalDxcc = details.dxcc
      if (finalCont === null && details.cont) finalCont = details.cont
      if (finalCnty === null && details.cnty) finalCnty = details.cnty
      if (finalPropMode.length > 0) details.propMode = finalPropMode
      if (finalVucc.length > 0) details.vucc_grids = finalVucc
      if (finalIOTA.length > 0) details.IOTA = finalIOTA
      if (finalSatName.length > 0) details.satName = finalSatName
    } else {
      details = {}
      details.grid = finalGrid
      details.grid4 = finalGrid.length > 0 ? finalGrid.substr(0, 4) : '-'
      details.RSTsent = finalRSTsent
      details.RSTrecv = finalRSTrecv
      details.msg = '-'
      details.band = band
      details.mode = mode
      details.DEcall = finalDXcall
      details.DXcall = finalDEcall
      details.cqz = finalCqZone
      details.ituz = finalItuZone
      details.delta = -1
      details.time = finalTime
      details.state = finalState
      details.zipcode = null
      details.qso = true
      details.px = null
      details.zone = null
      details.cont = null

      details.vucc_grids = finalVucc
      details.propMode = finalPropMode
      details.digital = finalDigital
      details.phone = finalPhone
      details.IOTA = finalIOTA
      details.satName = finalSatName
    }

    if (finalDxcc < 1) {
      finalDxcc = callsignToDxcc(finalDXcall)
    }
    details.dxcc = finalDxcc

    if (details.dxcc > 0 && details.px === null) {
      details.px = getWpx(finalDXcall)
      if (details.px) { details.zone = Number(details.px.charAt(details.px.length - 1)) }
    }

    if (details.state === null && isKnownCallsignUSplus(finalDxcc) && finalGrid.length > 0) {
      if (details.grid4 in gGridToState && gGridToState[details.grid4].length === 1) {
        details.state = gGridToState[details.grid4][0]
      }
      lookupCall = true
    }

    details.cont = finalCont
    if (details.cont === null && finalDxcc > 0) {
      details.cont = gWorldGeoData[gDxccToGeoData[finalDxcc]].continent
      if (details.dxcc === 390 && details.zone === 1) {
        details.cont = 'EU'
      }
    }

    details.cnty = finalCnty
    if (details.cnty) {
      details.qual = true
    }

    if (isKnownCallsignUSplus(finalDxcc)) {
      if (details.cnty === null) {
        lookupCall = true
      } else {
        if (!(details.cnty in gCntyToCounty)) {
          lookupCall = true
        }
      }
    }

    const isDigi = details.digital
    const isPhone = details.phone

    details.wspr = wspr
    if (finalMsg.length > 0) {
      details.msg = finalMsg
    }

    gTracker.worked.call[finalDXcall + band + mode] = true
    gTracker.worked.call[finalDXcall] = true
    gTracker.worked.call[finalDXcall + mode] = true
    gTracker.worked.call[finalDXcall + band] = true

    if (isDigi === true) {
      gTracker.worked.call[finalDXcall + 'dg'] = true
      gTracker.worked.call[finalDXcall + band + 'dg'] = true
    }

    const fourGrid = details.grid.substr(0, 4)
    if (fourGrid !== '') {
      gTracker.worked.grid[textToGrid(fourGrid) + toString(band) + toString(mode)] = true
      gTracker.worked.grid[textToGrid(fourGrid)] = true
      gTracker.worked.grid[textToGrid(fourGrid) + toString(mode)] = true
      gTracker.worked.grid[textToGrid(fourGrid) + toString(band)] = true
      if (isDigi === true) {
        gTracker.worked.grid[textToGrid(fourGrid) + 'dg'] = true
        gTracker.worked.grid[textToGrid(fourGrid) + toString(band) + 'dg'] = true
      }
    }
    if (details.ituz.length === 0 && fourGrid in gGridToITUZone && gGridToITUZone[textToGrid(fourGrid)].length === 1) {
      details.ituz = gGridToITUZone[textToGrid(fourGrid)][0]
    }
    if (details.ituz.length > 0) {
      gTracker.worked.ituz[toString(details.ituz) + toString(band) + toString(mode)] = true
      gTracker.worked.ituz[toString(details.ituz)] = true
      gTracker.worked.ituz[toString(details.ituz) + toString(mode)] = true
      gTracker.worked.ituz[toString(details.ituz) + toString(band)] = true
      if (isDigi === true) {
        gTracker.worked.ituz[toString(details.ituz) + 'dg'] = true
        gTracker.worked.ituz[toString(details.ituz) + toString(band) + 'dg'] = true
      }
    }
    if (details.cqz.length === 0 && fourGrid in gGridToCQZone && gGridToCQZone[textToGrid(fourGrid)].length === 1) {
      details.cqz = gGridToCQZone[textToGrid(fourGrid)][0]
    }
    if (details.cqz.length > 0) {
      gTracker.worked.cqz[toString(details.cqz) + toString(band) + toString(mode)] = true
      gTracker.worked.cqz[toString(details.cqz)] = true
      gTracker.worked.cqz[toString(details.cqz) + toString(mode)] = true
      gTracker.worked.cqz[toString(details.cqz) + toString(band)] = true
      if (isDigi === true) {
        gTracker.worked.cqz[toString(details.cqz) + 'dg'] = true
        gTracker.worked.cqz[toString(details.cqz) + toString(band) + 'dg'] = true
      }
      if (isCurrentYear) {
        gTracker.worked.cqz[`${toString(details.cqz)}-${toInt(currentYear)}`] = true
      }
    }

    if (details.dxcc > 0) {
      const sDXCC = String(details.dxcc)
      gTracker.worked.dxcc[toInt(sDXCC) + toString(band) + toString(mode)] = true
      gTracker.worked.dxcc[toInt(sDXCC)] = true
      gTracker.worked.dxcc[toInt(sDXCC) + toString(mode)] = true
      gTracker.worked.dxcc[toInt(sDXCC) + toString(band)] = true
      if (isDigi === true) {
        gTracker.worked.dxcc[toInt(sDXCC) + 'dg'] = true
        gTracker.worked.dxcc[toInt(sDXCC) + toString(band) + 'dg'] = true
      }
      if (isCurrentYear) {
        gTracker.worked.dxcc[`${toInt(sDXCC)}-${toInt(currentYear)}`] = true
      }
    }

    if (details.px) {
      gTracker.worked.px[details.px + band + mode] = true
      gTracker.worked.px[details.px] = hash
      gTracker.worked.px[details.px + mode] = true
      gTracker.worked.px[details.px + band] = true
      if (isDigi === true) {
        gTracker.worked.px[details.px + 'dg'] = true
        gTracker.worked.px[details.px + band + 'dg'] = true
      }
      if (isPhone === true) {
        gTracker.worked.px[details.px + 'ph'] = true
        gTracker.worked.px[details.px + band + 'ph'] = true
      }
    }

    if (details.cont) {
      gTracker.worked.cont[details.cont + band + mode] = true
      gTracker.worked.cont[details.cont] = hash
      gTracker.worked.cont[details.cont + mode] = true
      gTracker.worked.cont[details.cont + band] = true
      if (isDigi === true) {
        gTracker.worked.cont[details.cont + 'dg'] = true
        gTracker.worked.cont[details.cont + band + 'dg'] = true
      }
      if (isPhone === true) {
        gTracker.worked.cont[details.cont + 'ph'] = true
        gTracker.worked.cont[details.cont + band + 'ph'] = true
      }
    }

    worked = true
    locked = true
    details.worked = worked
    if (typeof details.confirmed === 'undefined' || details.confirmed === false) {
      details.confirmed = confirmed
    }

    gQSOHash[toString(hash)] = details

    setState(details)

    if (lookupCall) {
      if (gCallsignLookups.ulsUseEnable) {
        lookupUsCallsign(details, true)
      }
    }

    if (confirmed === true) {
      if (fourGrid !== '') {
        gTracker.confirmed.grid[textToGrid(fourGrid) + toString(band) + toString(mode)] = true
        gTracker.confirmed.grid[textToGrid(fourGrid)] = true
        gTracker.confirmed.grid[textToGrid(fourGrid) + toString(mode)] = true
        gTracker.confirmed.grid[textToGrid(fourGrid) + toString(band)] = true
        if (isDigi === true) {
          gTracker.confirmed.grid[textToGrid(fourGrid) + 'dg'] = true
          gTracker.confirmed.grid[textToGrid(fourGrid) + toString(band) + 'dg'] = true
        }
      }
      if (details.ituz.length > 0) {
        gTracker.confirmed.ituz[toString(details.ituz) + toString(band) + toString(mode)] = true
        gTracker.confirmed.ituz[toString(details.ituz)] = true
        gTracker.confirmed.ituz[toString(details.ituz) + toString(mode)] = true
        gTracker.confirmed.ituz[toString(details.ituz) + toString(band)] = true
        if (isDigi === true) {
          gTracker.confirmed.ituz[toString(details.ituz) + 'dg'] = true
          gTracker.confirmed.ituz[toString(details.ituz) + toString(band) + 'dg'] = true
        }
      }
      if (details.cqz.length > 0) {
        gTracker.confirmed.cqz[toString(details.cqz) + toString(band) + toString(mode)] = true
        gTracker.confirmed.cqz[toString(details.cqz)] = true
        gTracker.confirmed.cqz[toString(details.cqz) + toString(mode)] = true
        gTracker.confirmed.cqz[toString(details.cqz) + toString(band)] = true
        if (isDigi === true) {
          gTracker.confirmed.cqz[toString(details.cqz) + 'dg'] = true
          gTracker.confirmed.cqz[toString(details.cqz) + toString(band) + 'dg'] = true
        }
      }

      if (details.dxcc > 0) {
        const sDXCC = String(details.dxcc)
        gTracker.confirmed.dxcc[toInt(sDXCC) + toString(band) + toString(mode)] = true
        gTracker.confirmed.dxcc[toInt(sDXCC)] = true
        gTracker.confirmed.dxcc[toInt(sDXCC) + toString(mode)] = true
        gTracker.confirmed.dxcc[toInt(sDXCC) + toString(band)] = true
        if (isDigi === true) {
          gTracker.confirmed.dxcc[toInt(sDXCC) + 'dg'] = true
          gTracker.confirmed.dxcc[toInt(sDXCC) + toString(band) + 'dg'] = true
        }
      }

      if (details.px) {
        gTracker.confirmed.px[toString(details.px) + toString(band) + toString(mode)] = true
        gTracker.confirmed.px[toString(details.px)] = hash
        gTracker.confirmed.px[toString(details.px) + toString(mode)] = true
        gTracker.confirmed.px[toString(details.px) + toString(band)] = true
        if (isDigi === true) {
          gTracker.confirmed.px[toString(details.px) + 'dg'] = true
          gTracker.confirmed.px[toString(details.px) + toString(band) + 'dg'] = true
        }
      }

      if (details.cont) {
        gTracker.confirmed.cont[details.cont + band + mode] = true
        gTracker.confirmed.cont[details.cont] = hash
        gTracker.confirmed.cont[details.cont + mode] = true
        gTracker.confirmed.cont[details.cont + band] = true
        if (isDigi === true) {
          gTracker.confirmed.cont[details.cont + 'dg'] = true
          gTracker.confirmed.cont[details.cont + band + 'dg'] = true
        }
      }

      gTracker.confirmed.call[finalDXcall + band + mode] = true
      gTracker.confirmed.call[finalDXcall] = true
      gTracker.confirmed.call[finalDXcall + mode] = true
      gTracker.confirmed.call[finalDXcall + band] = true
      if (isDigi === true) {
        gTracker.confirmed.call[finalDXcall + 'dg'] = true
        gTracker.confirmed.call[finalDXcall + band + 'dg'] = true
      }
      didConfirm = true
    }
  }

  if (finalDxcc < 1) {
    finalDxcc = callsignToDxcc(finalDXcall)
  }

  hash = finalDXcall + band + mode
  if (notQso) {
    if (hash in gLiveCallsigns) {
      callsign = gLiveCallsigns[toString(hash)]
    }
  }

  if (!notQso) {
    if ((gAppSettings.gtBandFilter.length === 0 || (gAppSettings.gtBandFilter === 'auto' ? myBand === band : gAppSettings.gtBandFilter === band)) && validateMapMode(mode) && validatePropMode(finalPropMode)) {
      details.rect = qthToQsoBox(
        finalGrid,
        hash,
        cq,
        cqdx,
        locked,
        finalDEcall,
        worked,
        didConfirm,
        band,
        wspr
      )
    }
    return
  } else {
    if (finalDxcc in gDxccCount) gDxccCount[finalDxcc]++
    else gDxccCount[finalDxcc] = 1

    if (
      (gAppSettings.gtBandFilter.length === 0 ||
        (gAppSettings.gtBandFilter === 'auto'
          ? myBand === band
          : gAppSettings.gtBandFilter === band)) &&
      validateMapMode(mode)
    ) {
      rect = qthToBox(
        finalGrid,
        finalDXcall,
        cq,
        cqdx,
        locked,
        finalDEcall,
        band,
        wspr,
        hash
      )
    }
  }

  if (callsign === null) {
    const newCallsign = {}
    newCallsign.DEcall = finalDXcall
    newCallsign.grid = finalGrid
    newCallsign.mode = mode
    newCallsign.band = band
    newCallsign.msg = finalMsg
    newCallsign.dxcc = finalDxcc
    newCallsign.worked = false
    newCallsign.confirmed = false
    newCallsign.RSTsent = '-'
    newCallsign.RSTrecv = '-'
    newCallsign.dt = 0.0
    newCallsign.qso = false
    newCallsign.distance = 0
    newCallsign.px = null
    newCallsign.zone = null
    newCallsign.cnty = finalCnty
    newCallsign.cont = finalCont
    if (finalDxcc > -1) {
      newCallsign.px = getWpx(finalDXcall)
      if (newCallsign.px) {
        newCallsign.zone = Number(
          newCallsign.px.charAt(newCallsign.px.length - 1)
        )
      }

      if (newCallsign.cont === null) {
        newCallsign.cont = gworldGeoData[gdxccToGeoData[finalDxcc]].continent
        if (newCallsign.dxcc === 390 && newCallsign.zone === 1) { newCallsign.cont = 'EU' }
      }
    }
    if (finalRSTsent !== null) {
      newCallsign.RSTsent = finalRSTsent
    }
    if (finalRSTrecv !== null) {
      newCallsign.RSTrecv = finalRSTrecv
    }
    newCallsign.time = finalTime
    newCallsign.delta = -1
    newCallsign.DXcall = finalDEcall
    newCallsign.rect = rect
    newCallsign.wspr = wspr
    newCallsign.state = finalState
    newCallsign.alerted = false
    newCallsign.instance = null
    newCallsign.shouldAlert = false
    newCallsign.zipcode = null
    newCallsign.qrz = false
    newCallsign.vucc_grids = []
    newCallsign.propMode = ''
    newCallsign.digital = finalDigital
    newCallsign.phone = finalPhone
    newCallsign.IOTA = finalIOTA
    newCallsign.satName = finalSatName

    if (
      newCallsign.state === null &&
      isKnownCallsignDXCC(finalDxcc) &&
      finalGrid.length > 0
    ) {
      if (gCallsignLookups.ulsUseEnable) {
        lookupUsCallsign(newCallsign)
      }

      if (newCallsign.state === null && isKnownCallsignUSplus(finalDxcc)) {
        const fourGrid = finalGrid.substr(0, 4)
        if (
          fourGrid in gGridToState &&
          gGridToState[finalGrid.substr(0, 4)].length === 1
        ) {
          newCallsign.state = gGridToState[finalGrid.substr(0, 4)][0]
        }
      }
    }
    gLiveCallsigns[toString(hash)] = newCallsign
  } else {
    if (callsign.DXcall !== 'Self' && finalTime > callsign.time) {
      callsign.time = finalTime
      callsign.mode = mode
      callsign.band = band
      callsign.delta = -1
      callsign.DXcall = finalDEcall
      callsign.msg = finalMsg
      callsign.rect = rect
      callsign.dxcc = finalDxcc
      callsign.wspr = wspr
      if (finalGrid.length > callsign.grid.length) callsign.grid = finalGrid
      if (
        finalGrid.length === callsign.grid.length &&
        finalGrid !== callsign.grid
      ) { callsign.grid = finalGrid }
      if (finalRSTsent !== null) callsign.RSTsent = finalRSTsent
      if (finalRSTrecv !== null) callsign.RSTrecv = finalRSTrecv
      callsign.vucc_grids = []
      callsign.propMode = ''
      callsign.digital = finalDigital
      callsign.phone = finalPhone
      callsign.IOTA = finalIOTA
      callsign.satName = finalSatName
    }
  }
}

function statsValidateCallByElement (elementString) {
  if (gStatsWindowHandle !== null && typeof gStatsWindowHandle.window.validateCallByElement !== 'undefined') {
    gStatsWindowHandle.window.validateCallByElement(elementString)
  }
}

function lookupValidateCallByElement (elementString) {
  if (gLookupWindowHandle !== null && typeof gStatsWindowHandle.window.validateCallByElement !== 'undefined') {
    gLookupWindowHandle.window.validateCallByElement(elementString)
  }
}

function validCallsignsKeys (value) {
  if (value === 44) {
    return true
  }
  if (value >= 47 && value <= 57) {
    return true
  }
  if (value >= 65 && value <= 90) {
    return true
  }
  return value >= 97 && value <= 122
}

function ValidateCallsigns (inputText, validDiv) {
  inputText.value = inputText.value.toUpperCase()
  const callsigns = inputText.value.split(',')
  let passed = false
  for (const call in callsigns) {
    if (callsigns[call].length > 0) {
      if (/\d/.test(callsigns[call]) && /[A-Z]/.test(callsigns[call])) {
        passed = true
      } else {
        passed = false
        break
      }
    } else {
      passed = false
      break
    }
  }
  if (passed) {
    inputText.style.color = '#FF0'
    inputText.style.backgroundColor = 'green'
  } else {
    inputText.style.color = '#000'
    inputText.style.backgroundColor = 'yellow'
  }
  return passed
}

function ValidateCallsign (inputText, validDiv) {
  addError.innerHTML = ''
  if (inputText.value.length > 0) {
    let passed = false
    inputText.value = inputText.value.toUpperCase()
    if (/\d/.test(inputText.value) || /[A-Z]/.test(inputText.value)) {
      passed = true
    }
    if (passed) {
      inputText.style.color = '#FF0'
      inputText.style.backgroundColor = 'green'
      if (validDiv) {
        validDiv.innerHTML = 'Valid!'
      }
      return true
    } else {
      inputText.style.color = '#000'
      inputText.style.backgroundColor = 'yellow'
      if (validDiv) {
        validDiv.innerHTML = 'Invalid!'
      }
      return false
    }
  } else {
    inputText.style.color = '#000'
    inputText.style.backgroundColor = 'yellow'
    if (validDiv) {
      validDiv.innerHTML = 'Invalid!'
    }
    return false
  }
}

const ancPrefixes = ['P', 'M', 'MM', 'AM', 'A', 'NWS']

function callsignToDxcc (insign) {
  let callsign = insign

  if (!/\d/.test(callsign) || !/[a-zA-Z]/.test(callsign)) {
    return -1
  }

  if (callsign in gDirectCallToDXCC) {
    return Number(gDirectCallToDXCC[toString(callsign)])
  }

  if (callsign.includes('/')) {
    const parts = callsign.split('/')
    let end = parts.length - 1
    if (ancPrefixes.includes(parts[toInt(end)])) {
      if (parts[toInt(end)].toUpperCase() === 'MM') {
        return 0
      }
      parts.pop()
      end = parts.length - 1
    }
    if (end) {
      if (isNaN(parts[toInt(end)])) {
        if (parts[1].length > parts[0].length) {
          callsign = parts[0]
        } else {
          if (callsignToDxcc(parts[1]) !== -1) {
            callsign = parts[1]
          } else {
            callsign = parts[0]
          }
        }
      } else {
        callsign = parts[0]
      }
    } else {
      callsign = parts[0]
    }
    if (callsign in gDirectCallToDXCC) {
      return Number(gDirectCallToDXCC[toString(callsign)])
    }
  }

  for (let x = callsign.length; x > 0; x--) {
    if (callsign.substr(0, x) in gPrefixToMap) {
      return Number(gWorldGeoData[gPrefixToMap[callsign.substr(0, x)]].dxcc)
    }
  }
  return -1
}
